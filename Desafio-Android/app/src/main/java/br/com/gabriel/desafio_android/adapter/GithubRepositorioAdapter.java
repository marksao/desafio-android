package br.com.gabriel.desafio_android.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.gabriel.desafio_android.R;
import br.com.gabriel.desafio_android.activity.PullRequest;
import br.com.gabriel.desafio_android.model.Item;

/**
 * Created by Gabriel on 20/06/2017.
 */

public class GithubRepositorioAdapter  extends RecyclerView.Adapter<GithubRepositorioAdapter.GitHubRepositorioViewHolder>  {

    private List<Item> items;
    private Context context;

    public GithubRepositorioAdapter(List<Item> items) {
        this.items = items;
    }

    @Override
    public GithubRepositorioAdapter.GitHubRepositorioViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_repositorio, parent, false);
        context = parent.getContext();
        return new GithubRepositorioAdapter.GitHubRepositorioViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final GithubRepositorioAdapter.GitHubRepositorioViewHolder holder, int position) {
        final Item item = getItem(position);

        try {
            holder.tv_descricao_repositorio.setText(String.format("%s", item.getDescription()));
            holder.tv_forks.setText(String.format("%s", item.getForks()));
            holder.tv_stars.setText(String.format("%s", item.getStargazersCount()));
            holder.tv_nome_autor.setText(String.format("%s", item.getOwner().getLogin()));
            holder.tv_nome_repositorio.setText(String.format("%s", item.getName()));
            Picasso.with(context).load(item.getOwner().getAvatarUrl()).into(holder.iv_foto_autor);
            holder.cv_repositorio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, PullRequest.class);
                    intent.putExtra("model",item);
                    context.startActivity(intent);
                }
            });

        }catch (Exception e){
            Log.e("Gitgruberror",""+e.getCause());
        }
    }

    private Item getItem(int pos){
        return items.get(pos);
    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }

    class GitHubRepositorioViewHolder extends RecyclerView.ViewHolder  {

        private CardView cv_repositorio;

        private TextView tv_nome_repositorio;
        private TextView tv_descricao_repositorio;
        private TextView tv_nome_autor;
        private ImageView iv_foto_autor;

        private TextView tv_stars;
        private TextView tv_forks;

        public GitHubRepositorioViewHolder(View itemView) {
            super(itemView);
            iv_foto_autor = (ImageView) itemView.findViewById(R.id.iv_foto_autor);
            tv_descricao_repositorio = (TextView) itemView.findViewById(R.id.tv_descricao_repositorio);
            tv_nome_repositorio = (TextView) itemView.findViewById(R.id.tv_nome_repositorio);
            tv_nome_autor = (TextView) itemView.findViewById(R.id.tv_nome_autor);
            tv_stars = (TextView) itemView.findViewById(R.id.tv_stars);
            tv_forks = (TextView) itemView.findViewById(R.id.tv_fork);
            cv_repositorio = (CardView) itemView.findViewById(R.id.cv_repositorio);
        }
    }
}
