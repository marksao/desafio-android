package br.com.gabriel.desafio_android.activity;

import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import br.com.gabriel.desafio_android.R;
import br.com.gabriel.desafio_android.adapter.GithubRepositorioAdapter;
import br.com.gabriel.desafio_android.adapter.PullRequestAdapter;
import br.com.gabriel.desafio_android.model.Item;
import br.com.gabriel.desafio_android.model.pulls.Pull;
import br.com.gabriel.desafio_android.service.GithubService;

public class PullRequest extends AppCompatActivity implements IPullRequest {

    private Item item;
    private Toolbar barra;
    private RecyclerView rv_pulls;
    private PullRequestAdapter pullRequestAdapter;
    private ArrayList<Pull> items = new ArrayList<>();
    private ImageView iv_arrow_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_request);

        setupBar();
        setupRecyclerView();
        setupBackButton();

        if(savedInstanceState == null){
            Bundle bundle = getIntent().getExtras();
            item = bundle.getParcelable("model");
            new GithubService(this).listarPullRequests(item.getOwner().getLogin(),item.getName());
        }else{
            items = savedInstanceState.getParcelableArrayList("key");
            PullRequestAdapter githubRepositorioAdapter = new PullRequestAdapter(items);
            rv_pulls.setAdapter(githubRepositorioAdapter);
        }


    }

    private void setupBackButton(){
        iv_arrow_back = (ImageView) findViewById(R.id.iv_arrow_back);
        iv_arrow_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void setupBar(){
        barra = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(barra);
        try {
            getSupportActionBar().setTitle("");
        }catch (Exception e){
            e.getCause();
        }
    }

    private void setupRecyclerView(){
        rv_pulls = (RecyclerView) findViewById(R.id.rv_pulls);

        rv_pulls.setHasFixedSize(true);
        LinearLayoutManager glm = new LinearLayoutManager(PullRequest.this);
        glm.setOrientation(LinearLayoutManager.VERTICAL);
        rv_pulls.setLayoutManager(glm);
    }

    @Override
    public void montarPullRequests(List<Pull> pulls) {
        items.addAll(pulls);
        pullRequestAdapter = new PullRequestAdapter(pulls);
        rv_pulls.setAdapter(pullRequestAdapter);
    }

    @Override
    public void erroRequest() {

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList("key", items);
        super.onSaveInstanceState(outState);
    }
}
