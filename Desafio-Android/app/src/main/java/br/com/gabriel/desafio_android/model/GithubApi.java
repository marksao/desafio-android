
package br.com.gabriel.desafio_android.model;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GithubApi implements Parcelable
{

    @SerializedName("total_count")
    @Expose
    private Integer totalCount;
    @SerializedName("incomplete_results")
    @Expose
    private Boolean incompleteResults;
    @SerializedName("items")
    @Expose
    private List<Item> items = null;
    public final static Creator<GithubApi> CREATOR = new Creator<GithubApi>() {


        @SuppressWarnings({
            "unchecked"
        })
        public GithubApi createFromParcel(Parcel in) {
            GithubApi instance = new GithubApi();
            instance.totalCount = ((Integer) in.readValue((Integer.class.getClassLoader())));
            instance.incompleteResults = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
            in.readList(instance.items, (br.com.gabriel.desafio_android.model.Item.class.getClassLoader()));
            return instance;
        }

        public GithubApi[] newArray(int size) {
            return (new GithubApi[size]);
        }

    }
    ;

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Boolean getIncompleteResults() {
        return incompleteResults;
    }

    public void setIncompleteResults(Boolean incompleteResults) {
        this.incompleteResults = incompleteResults;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(totalCount);
        dest.writeValue(incompleteResults);
        dest.writeList(items);
    }

    public int describeContents() {
        return  0;
    }

}
