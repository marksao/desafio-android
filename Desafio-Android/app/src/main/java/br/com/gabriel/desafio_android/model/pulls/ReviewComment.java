
package br.com.gabriel.desafio_android.model.pulls;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReviewComment implements Parcelable
{

    @SerializedName("href")
    @Expose
    private String href;
    public final static Creator<ReviewComment> CREATOR = new Creator<ReviewComment>() {


        @SuppressWarnings({
            "unchecked"
        })
        public ReviewComment createFromParcel(Parcel in) {
            ReviewComment instance = new ReviewComment();
            instance.href = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public ReviewComment[] newArray(int size) {
            return (new ReviewComment[size]);
        }

    }
    ;

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(href);
    }

    public int describeContents() {
        return  0;
    }

}
