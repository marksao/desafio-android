package br.com.gabriel.desafio_android.service;

import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import br.com.gabriel.desafio_android.activity.IMainActivity;
import br.com.gabriel.desafio_android.activity.IPullRequest;
import br.com.gabriel.desafio_android.activity.MainActivity;
import br.com.gabriel.desafio_android.adapter.GithubRepositorioAdapter;
import br.com.gabriel.desafio_android.infra.Server;
import br.com.gabriel.desafio_android.model.GithubApi;
import br.com.gabriel.desafio_android.model.Item;
import br.com.gabriel.desafio_android.model.pulls.Pull;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class GithubService {

    private IMainActivity mainActivity;
    private IPullRequest pullRequest;

    public GithubService(IMainActivity mainActivity){
        this.mainActivity=mainActivity;
    }
    public GithubService(IPullRequest pullRequest){
        this.pullRequest=pullRequest;
    }

    /*
    É sempre acionado quando há uma rolagem (paginação) de dados
     */
    public void listarRepositorios(final int page, final int totalItens, final RecyclerView rcView, final List<Item> dadosAtuais, final int inicializar){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Server.URL_SEARCH)
                .client(getClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        IGithubService iGithubService = retrofit.create(IGithubService.class);

        Call<GithubApi> request = iGithubService.listarRepositorios("language:Java","stars",page);

        request.enqueue(new Callback<GithubApi>() {
            @Override
            public void onResponse(Call<GithubApi> call, Response<GithubApi> response) {
                dadosAtuais.addAll(response.body().getItems());
                if(inicializar==0){
                    GithubRepositorioAdapter bpa = new GithubRepositorioAdapter(dadosAtuais);
                    rcView.setAdapter(bpa);
                }else {
                    rcView.post(new Runnable() {
                        @Override
                        public void run() {
                            GithubRepositorioAdapter bpa = (GithubRepositorioAdapter) rcView.getAdapter();
                            bpa.notifyItemRangeInserted(bpa.getItemCount(), dadosAtuais.size() - 1);
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<GithubApi> call, Throwable t) {
                if(t instanceof SocketTimeoutException){listarRepositorios(page,totalItens,rcView,dadosAtuais,inicializar);}
            }
        });
    }

    /*
    Acionado quando busco os pulls do criador/repositorio
     */
    public void listarPullRequests(final String criador, final String repositorio){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Server.URL_PROJETO)
                .client(getClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        IGithubService iGithubService = retrofit.create(IGithubService.class);
        Call<List<Pull>> request = iGithubService.listarPullRequests(criador,repositorio);

        request.enqueue(new Callback<List<Pull>>() {
            @Override
            public void onResponse(Call<List<Pull>> call, Response<List<Pull>> response) {
                pullRequest.montarPullRequests(response.body());
            }

            @Override
            public void onFailure(Call<List<Pull>> call, Throwable t) {
                if(t instanceof SocketTimeoutException){listarPullRequests(criador,repositorio);}
            }
        });
    }

    /*
    Retorna sempre uma conexão para ser utilizada no retrofit
    * * reconexão por timeout
     */
    private OkHttpClient getClient(){
        return new OkHttpClient.Builder()
                .connectTimeout(2, TimeUnit.SECONDS)
                .readTimeout(10,TimeUnit.SECONDS)
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request request = chain.request();
                        request = request.newBuilder()
                                .header("Cache-Control",String.format("max-age=%d",7200))
                                .build();
                        okhttp3.Response response = chain.proceed(request);

                        int tryCount = 0;
                        while(!response.isSuccessful() && tryCount < 3){
                            Log.d("interceptor","Erro na requisição" + tryCount);
                            tryCount++;
                            response = chain.proceed(request);
                        }

                        return response;
                    }
                }).build();
    }


}