package br.com.gabriel.desafio_android.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.gabriel.desafio_android.R;
import br.com.gabriel.desafio_android.adapter.GithubRepositorioAdapter;
import br.com.gabriel.desafio_android.model.Item;
import br.com.gabriel.desafio_android.service.GithubService;
import br.com.gabriel.desafio_android.util.EndlessRecyclerViewScrollListener;

public class MainActivity extends AppCompatActivity implements IMainActivity{

    private RecyclerView rv_repositorios;
    private GithubRepositorioAdapter githubRepositorioAdapter;
    private EndlessRecyclerViewScrollListener scrollListener;
    private ArrayList<Item> items = new ArrayList<>();
    private Toolbar barra;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupBar();
        setupRecyclerView();

        if(savedInstanceState == null){
            new GithubService(this).listarRepositorios(1, 0, rv_repositorios, items,0);
        }else{
            items = savedInstanceState.getParcelableArrayList("key");
            GithubRepositorioAdapter githubRepositorioAdapter = new GithubRepositorioAdapter(items);
            rv_repositorios.setAdapter(githubRepositorioAdapter);
        }
    }

    private void setupBar(){
        barra = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(barra);
        try {
            getSupportActionBar().setTitle("Repositórios");
        }catch (Exception e){
            e.getCause();
        }
    }

    public void setupRecyclerView(){
        rv_repositorios = (RecyclerView) findViewById(R.id.rv_repositorios);

        LinearLayoutManager llm = new LinearLayoutManager(MainActivity.this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rv_repositorios.setLayoutManager(llm);

        scrollListener = new EndlessRecyclerViewScrollListener(llm) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                loadNextApi(page,totalItemsCount,view,items);
            }
        };

        rv_repositorios.addOnScrollListener(scrollListener);
    }

    public void loadNextApi(int page,int totalCount,RecyclerView rcView, List<Item> dados){
        new GithubService(this).listarRepositorios(page,totalCount,rcView,dados,1);
    }

    @Override
    public void erroGenerico() {
        Toast.makeText(MainActivity.this, "error", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList("key", items);
        super.onSaveInstanceState(outState);
    }
}
