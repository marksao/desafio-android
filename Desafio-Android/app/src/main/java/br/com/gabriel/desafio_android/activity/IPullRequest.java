package br.com.gabriel.desafio_android.activity;

import java.util.List;

import br.com.gabriel.desafio_android.model.pulls.Pull;

/**
 * Created by Gabriel on 20/06/2017.
 */

public interface IPullRequest {
    public void montarPullRequests(List<Pull> pulls);
    public void erroRequest();
}
