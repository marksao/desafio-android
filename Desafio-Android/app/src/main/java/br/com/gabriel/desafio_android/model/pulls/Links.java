
package br.com.gabriel.desafio_android.model.pulls;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Links implements Parcelable
{

    @SerializedName("self")
    @Expose
    private Self self;
    @SerializedName("html")
    @Expose
    private Html html;
    @SerializedName("issue")
    @Expose
    private Issue issue;
    @SerializedName("comments")
    @Expose
    private Comments comments;
    @SerializedName("review_comments")
    @Expose
    private ReviewComments reviewComments;
    @SerializedName("review_comment")
    @Expose
    private ReviewComment reviewComment;
    @SerializedName("commits")
    @Expose
    private Commits commits;
    @SerializedName("statuses")
    @Expose
    private Statuses statuses;
    public final static Creator<Links> CREATOR = new Creator<Links>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Links createFromParcel(Parcel in) {
            Links instance = new Links();
            instance.self = ((Self) in.readValue((Self.class.getClassLoader())));
            instance.html = ((Html) in.readValue((Html.class.getClassLoader())));
            instance.issue = ((Issue) in.readValue((Issue.class.getClassLoader())));
            instance.comments = ((Comments) in.readValue((Comments.class.getClassLoader())));
            instance.reviewComments = ((ReviewComments) in.readValue((ReviewComments.class.getClassLoader())));
            instance.reviewComment = ((ReviewComment) in.readValue((ReviewComment.class.getClassLoader())));
            instance.commits = ((Commits) in.readValue((Commits.class.getClassLoader())));
            instance.statuses = ((Statuses) in.readValue((Statuses.class.getClassLoader())));
            return instance;
        }

        public Links[] newArray(int size) {
            return (new Links[size]);
        }

    }
    ;

    public Self getSelf() {
        return self;
    }

    public void setSelf(Self self) {
        this.self = self;
    }

    public Html getHtml() {
        return html;
    }

    public void setHtml(Html html) {
        this.html = html;
    }

    public Issue getIssue() {
        return issue;
    }

    public void setIssue(Issue issue) {
        this.issue = issue;
    }

    public Comments getComments() {
        return comments;
    }

    public void setComments(Comments comments) {
        this.comments = comments;
    }

    public ReviewComments getReviewComments() {
        return reviewComments;
    }

    public void setReviewComments(ReviewComments reviewComments) {
        this.reviewComments = reviewComments;
    }

    public ReviewComment getReviewComment() {
        return reviewComment;
    }

    public void setReviewComment(ReviewComment reviewComment) {
        this.reviewComment = reviewComment;
    }

    public Commits getCommits() {
        return commits;
    }

    public void setCommits(Commits commits) {
        this.commits = commits;
    }

    public Statuses getStatuses() {
        return statuses;
    }

    public void setStatuses(Statuses statuses) {
        this.statuses = statuses;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(self);
        dest.writeValue(html);
        dest.writeValue(issue);
        dest.writeValue(comments);
        dest.writeValue(reviewComments);
        dest.writeValue(reviewComment);
        dest.writeValue(commits);
        dest.writeValue(statuses);
    }

    public int describeContents() {
        return  0;
    }

}
