package br.com.gabriel.desafio_android.service;

import java.util.List;

import br.com.gabriel.desafio_android.model.GithubApi;
import br.com.gabriel.desafio_android.model.pulls.Pull;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface IGithubService {
    @GET("repositories")
    Call<GithubApi> listarRepositorios(
            @Query("q") String language,
            @Query("sort") String sort,
            @Query("page") int page);

    @GET("repos/{criador}/{repositorio}/pulls")
    Call<List<Pull>> listarPullRequests(@Path("criador") String criador, @Path("repositorio") String repositorio);
}
