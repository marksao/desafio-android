
package br.com.gabriel.desafio_android.model.pulls;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Comments implements Parcelable
{

    @SerializedName("href")
    @Expose
    private String href;
    public final static Creator<Comments> CREATOR = new Creator<Comments>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Comments createFromParcel(Parcel in) {
            Comments instance = new Comments();
            instance.href = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public Comments[] newArray(int size) {
            return (new Comments[size]);
        }

    }
    ;

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(href);
    }

    public int describeContents() {
        return  0;
    }

}
