package br.com.gabriel.desafio_android.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.gabriel.desafio_android.R;
import br.com.gabriel.desafio_android.activity.PullRequest;
import br.com.gabriel.desafio_android.model.Item;
import br.com.gabriel.desafio_android.model.pulls.Pull;

/**
 * Created by Gabriel on 20/06/2017.
 */

public class PullRequestAdapter extends RecyclerView.Adapter<PullRequestAdapter.ViewHolder>  {

    private List<Pull> items;
    private Context context;

    public PullRequestAdapter(List<Pull> items) {
        this.items = items;
    }

    @Override
    public PullRequestAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pull, parent, false);
        context = parent.getContext();
        return new PullRequestAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final PullRequestAdapter.ViewHolder holder, int position) {
        final Pull item = getItem(position);

        try {
            holder.tv_body.setText(String.format("%s", item.getBody()));
            holder.tv_data.setText(String.format("%s", item.getCreatedAt()));
            holder.tv_nome_autor.setText(String.format("%s", item.getUser().getLogin()));
            holder.tv_titulo.setText(String.format("%s", item.getTitle()));
            Picasso.with(context).load(item.getUser().getAvatarUrl()).into(holder.iv_foto_autor);

            holder.cv_pull.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(item.getHtmlUrl()));
                    context.startActivity(i);
                }
            });


        }catch (Exception e){
            Log.e("Gitgruberror",""+e.getCause());
        }
    }

    private Pull getItem(int pos){
        return items.get(pos);
    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder  {

        private CardView cv_pull;

        private TextView tv_nome_autor;
        private TextView tv_data;
        private TextView tv_body;
        private TextView tv_titulo;
        private ImageView iv_foto_autor;

        public ViewHolder(View itemView) {
            super(itemView);
            iv_foto_autor = (ImageView) itemView.findViewById(R.id.iv_foto_autor);
            tv_titulo = (TextView) itemView.findViewById(R.id.tv_titulo);
            tv_body = (TextView) itemView.findViewById(R.id.tv_body);
            tv_data = (TextView) itemView.findViewById(R.id.tv_data);
            tv_nome_autor = (TextView) itemView.findViewById(R.id.tv_nome_autor);
            cv_pull = (CardView) itemView.findViewById(R.id.cv_pull);
        }
    }
}
