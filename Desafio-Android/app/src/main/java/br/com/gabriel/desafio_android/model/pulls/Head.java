
package br.com.gabriel.desafio_android.model.pulls;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Head implements Parcelable
{

    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("ref")
    @Expose
    private String ref;
    @SerializedName("sha")
    @Expose
    private String sha;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("repo")
    @Expose
    private Repo repo;
    public final static Creator<Head> CREATOR = new Creator<Head>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Head createFromParcel(Parcel in) {
            Head instance = new Head();
            instance.label = ((String) in.readValue((String.class.getClassLoader())));
            instance.ref = ((String) in.readValue((String.class.getClassLoader())));
            instance.sha = ((String) in.readValue((String.class.getClassLoader())));
            instance.user = ((User) in.readValue((User.class.getClassLoader())));
            instance.repo = ((Repo) in.readValue((Repo.class.getClassLoader())));
            return instance;
        }

        public Head[] newArray(int size) {
            return (new Head[size]);
        }

    }
    ;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getSha() {
        return sha;
    }

    public void setSha(String sha) {
        this.sha = sha;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Repo getRepo() {
        return repo;
    }

    public void setRepo(Repo repo) {
        this.repo = repo;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(label);
        dest.writeValue(ref);
        dest.writeValue(sha);
        dest.writeValue(user);
        dest.writeValue(repo);
    }

    public int describeContents() {
        return  0;
    }

}
